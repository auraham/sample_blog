#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Author'
SITENAME = 'Title'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public/'

TIMEZONE = 'America/Monterrey'
DEFAULT_LANG = 'en'
DEFAULT_DATE_FORMAT = '%d %B %Y'


# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True


# ---- PLUGINS ----
PLUGIN_PATHS = ["plugins"]
PLUGINS = ["render_math"]

#MARKUP = ("md", "ipynb")
#PLUGIN_PATHS = ["./plugins"]
#PLUGINS = ["render_math", "ipynb.markup"]
#IGNORE_FILES = [".ipynb_checkpoints"]

# ---- THEME ----
THEME = "themes/Flex-minimal"

# --- FLEX-MINIMAL: CONFIG ---
SHOW_ASIDE = False                              # hides left bar
CUSTOM_CSS = "theme/stylesheet/custom.css"      # custom css
MAIN_MENU = True                                # shows main upper menu
HOME_HIDE_TAGS = True                           # hides links in sidebar
LINKS_IN_NEW_TAB = 'external'
PYGMENTS_STYLE = 'rocket'                       # theme for syntax highlighting
USE_GOOGLE_FONTS = False

# --- FLEX-MINIMAL: LANDING PAGE ---
# check this url for creating a custom index.html page
# https://github.com/getpelican/pelican/issues/400
DIRECT_TEMPLATES = ['index', 'archives', 'blog']
INDEX_SAVE_AS = 'posts.html'            # now, the jinja template index.html serves as url/posts.html and displays all the posts
PAGE_SAVE_AS = '{slug}.html'            # be sure to save the landing page in content/page/index.md, and setup slug as 'Slug: index'
PAGE_URL = '{slug}.html'                # remove the use of 'page' in the url

# --- FLEX-MINIMAL: MENU ---
# note: we edit base.html to add SITEURL as follows:
#   <a href="{{ SITEURL }}/{{ link }}">{{ title }}</a>
# so, use ("Blog", "blog.html") instead of ("Blog", "/blog.html")
MENUITEMS = (
            ("Blog", "blog.html"),      # we use blog.html as an alias/copy of archives.html
            ("CV", "cv.html"),
            ("Contact", "contact.html"),
            )

