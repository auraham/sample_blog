---
Title: Software
Date: 2019-03-18 21:44
Category: software
Author: Auraham Camacho
---


**Very Fast Nondominated Sorting**

A concurrent implementation of VFNS in python.

`git clone https://gitlab.com/auraham/vfns.git`

[[code](https://gitlab.com/auraham/vfns)] 

---

**Diversity Comparison Indicator**

A python implementation of DCI.

`git clone https://gitlab.com/auraham/dci.git`

[[code](https://gitlab.com/auraham/dci)]

