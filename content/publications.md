---
Title: Publications
Date: 2019-03-18 21:41
Category: publications
Author: Auraham Camacho
---

**A Hybrid Surrogate-Assisted Evolutionary Algorithm for Computationally Expensive Many-Objective Optimization**

Kanzhen Wan, Cheng He, Auraham Camacho, Ke Shang, Rang Cheng, and Hisao Ishibuchi. *CEC 2019*.

[[paper](https://ieeexplore.ieee.org/document/8789913)]



**Indicator-based Weight Adaptation for Solving Many-Objective Optimization Problems**

Auraham Camacho, Gregorio Toscano, Ricardo Landa, and Hisao Ishibuchi. *EMO 2019*.

[[paper](https://link.springer.com/chapter/10.1007/978-3-030-12598-1_18)]  [[code](https://bitbucket.org/auraham_cg/mdp/src/default/)]
