---
Title: Auraham Camacho
Date: 2020-03-18 13:28
Slug: index
---

`acamacho` at `tamps.cinvestav.mx`

I received the Ph.D. degree from the [Center for Research and Advanced Studies](https://www.tamps.cinvestav.mx) (Cinvestav Unidad Tamaulipas), advised by [Dr. Gregorio Toscano](https://gtoscano.tech) and [Dr. Ricardo Landa](https://www.tamps.cinvestav.mx/~rlanda). I am interested in evolutionary algorithms, multi- and many-objective optimization.

Multiobjective evolutionary algorithms have been used for solving optimization problems since many years. Among them, decomposition-based algorithms are often employed for solving many-objective optimization problems, given their efficiency and scalability. I investigate how to improve the behavior of such algorithms. Along with my advisors, I am the co-author of the [multiple survival selection](#) (MSS), a strategy for improving the performance of decomposition in many-objective optimization.

## Timeline

- (2015-2020) **Ph.D.** [Cinvestav Unidad Tamaulipas](https://www.tamps.cinvestav.mx). Thesis: On the Use of Scalarizing Functions to Solve Many-Objective Optimization Problems. Advisors: Dr. Gregorio Toscano and Dr. Ricardo Landa.


- (2018-2019) **Resarch stay** [Southern University of Science and Technology](https://www.sustech.edu.cn/en).
Advisor: Dr. Hisao Ishibuchi.


- (2012-2014) **M.Sc.** [Cinvestav Unidad Tamaulipas](https://www.tamps.cinvestav.mx). Thesis: Study of hybrid schemes for the simultaneous use of different formulations of a multi-objective optimization problem. Advisor: Dr. Gregorio Toscano.

- (2007-2011) **B.S.** [Instituto Tecnológico de Matamoros](http://www.itmatamoros.edu.mx/).



## Publications

- (2020) **Another Difficulty of Inverted Triangular Pareto Fronts for Decomposition-Based Multi-Objective Algorithms**. Linjun He, Auraham Camacho, Hisao Ishibuchi. *GECCO 2020*.

- (2019) **A Hybrid Surrogate-Assisted Evolutionary Algorithm for Computationally Expensive Many-Objective Optimization**. Kanzhen Wan, Cheng He, Auraham Camacho, Ke Shang, Rang Cheng, and Hisao Ishibuchi. *CEC 2019*. [[paper](https://ieeexplore.ieee.org/document/8789913)]

- (2019) **Indicator-based Weight Adaptation for Solving Many-Objective Optimization Problems**. Auraham Camacho, Gregorio Toscano, Ricardo Landa, and Hisao Ishibuchi. *EMO 2019*. [[paper](https://link.springer.com/chapter/10.1007/978-3-030-12598-1_18)]  [[code](https://bitbucket.org/auraham_cg/mdp/src/default/)]

## Software

- **Very Fast Nondominated Sorting**. A concurrent implementation of VFNS in python. [[code](https://gitlab.com/auraham/vfns)] 

- **Diversity Comparison Indicator**. A python implementation of DCI. [[code](https://gitlab.com/auraham/dci)]


## Resources

- **Latex template for presentations**. This is the latex source code that I commonly use for my presentations. Feel free to use it. [[code](#)]






