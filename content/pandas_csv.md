---
Title: CSV files with Pandas
Date: 2020-03-18 13:28
Category: pandas
Author: Auraham Camacho
---

**update** This is the best approach:

```python
# this automatically reads the column names
df = pd.read_csv("sample_input.csv", sep=";")

# this also reads the column names AND get a subset of them
df = pd.read_csv("sample_input.csv", usecols=["m_objs", "A", "D"], sep=";")

# this is useful to ignore the comment in the first line
df = pd.read_csv("sample_input.csv", usecols=["m_objs", "A", "D"], sep=";", 
                 skiprows=1)
```

It is not necessary to use `names`.

https://data36.com/pandas-tutorial-1-basics-reading-data-files-dataframes-data-selection/

**update** If you got this error message, then you need to update `pandas`.

```
TypeError: The numpy boolean negative, the `-` operator, is not supported, use the `~` operator or the logical_not function instead.
```

According to [this post](<https://stackoverflow.com/questions/52274857/numpy-typeerror>), the problem is that you are using an old version of pandas with a new version of python. A quick fix is to include `usecols` when calling `read_csv`.

You can upgrade `pandas` as follows:

```
pip install --upgrade pandas
```

This is my `pip freeze`:

```
attrs==19.1.0
backcall==0.1.0
bleach==3.1.0
cairocffi==1.0.2
cffi==1.12.3
cycler==0.10.0
decorator==4.4.0
defusedxml==0.6.0
entrypoints==0.3
icdiff==1.9.1
ipdb==0.12
ipykernel==5.1.1
ipython==7.5.0
ipython-genutils==0.2.0
ipywidgets==7.4.2
jedi==0.13.3
Jinja2==2.10.1
jsonschema==3.0.1
jupyter==1.0.0
jupyter-client==5.2.4
jupyter-console==6.0.0
jupyter-core==4.4.0
MarkupSafe==1.1.1
matplotlib==1.5.0
mistune==0.8.4
nbconvert==5.5.0
nbformat==4.4.0
notebook==5.7.8
numpy==1.16.4
pandas==0.24.2
pandocfilters==1.4.2
parso==0.4.0
pexpect==4.7.0
pgi==0.0.11.2
pickleshare==0.7.5
prometheus-client==0.6.0
prompt-toolkit==2.0.9
ptyprocess==0.6.0
pycairo==1.18.1
pycparser==2.19
Pygments==2.4.2
PyGObject==3.32.1
pyparsing==2.4.0
pyrsistent==0.15.2
python-dateutil==2.8.0
pytz==2019.1
pyzmq==18.0.1
qtconsole==4.5.1
scipy==1.3.0
Send2Trash==1.5.0
six==1.12.0
terminado==0.8.2
testpath==0.4.2
tornado==6.0.2
traitlets==4.3.2
wcwidth==0.1.7
webencodings==0.5.1
widgetsnbextension==3.4.2

```







## Full example

```python
# pandas_csv.py
from __future__ import print_function
import numpy as np
import pandas as pd

if __name__ == "__main__":

    """
    Problem;m_objs;A;B;C;D
    DTLZ1;3;0.01;0.07;0.13;0.19
    DTLZ1;3;0.02;0.08;0.14;0.2
    DTLZ1;5;0.03;0.09;0.15;0.21
    DTLZ1;5;0.04;0.1;0.16;0.22
    DTLZ1;8;0.05;0.11;0.17;0.23
    DTLZ1;8;0.06;0.12;0.18;0.24
    """

    # create sample data
    data = {
        "Problem": ["DTLZ1", "DTLZ1", "DTLZ1", "DTLZ1", "DTLZ1", "DTLZ1"], 
        "m_objs": [3, 3, 5, 5, 8, 8],
        "A": [0.01, 0.02, 0.03, 0.04, 0.05, 0.06],
        "B": [0.07, 0.08, 0.09, 0.10, 0.11, 0.12],
        "C": [0.13, 0.14, 0.15, 0.16, 0.17, 0.18],
        "D": [0.19, 0.20, 0.21, 0.22, 0.23, 0.24],
        }
    
    # create data frame
    df = pd.DataFrame(data, 
                      columns=["Problem", "m_objs", "A", "B", "C", "D"])
    
    # save to csv
    df.to_csv("sample_input.csv", index=False, sep=";")
    
    # load csv (all columns)
    df_all = pd.read_csv("sample_input.csv", sep=";")
    
    # load csv (some columns)
    df_some = pd.read_csv("sample_input.csv", sep=";", 
                          usecols=["Problem", "m_objs", "D"])
    
    # load csv with one comment in the first line (all columns)
    df_all_c = pd.read_csv("sample_input_with_comments.csv", 
                           skiprows=1, sep=";")
    
    # load csv with one comment in the first line (some columns)
    df_some_c = pd.read_csv("sample_input_with_comments.csv", 
                            skiprows=1, sep=";", 
                            usecols=["Problem", "m_objs", "D"])
```



## Create file

Consider this file, called `sample_input.csv`:

```
Problem;m_objs;A;B;C;D
DTLZ1;3;0.01;0.07;0.13;0.19
DTLZ1;3;0.02;0.08;0.14;0.2
DTLZ1;5;0.03;0.09;0.15;0.21
DTLZ1;5;0.04;0.1;0.16;0.22
DTLZ1;8;0.05;0.11;0.17;0.23
DTLZ1;8;0.06;0.12;0.18;0.24
```

To create this file from python, use this snippet:

```python
# create sample data
data = {
        "Problem": ["DTLZ1", "DTLZ1", "DTLZ1", "DTLZ1", "DTLZ1", "DTLZ1"], 
        "m_objs": [3, 3, 5, 5, 8, 8],
        "A": [0.01, 0.02, 0.03, 0.04, 0.05, 0.06],
        "B": [0.07, 0.08, 0.09, 0.10, 0.11, 0.12],
        "C": [0.13, 0.14, 0.15, 0.16, 0.17, 0.18],
        "D": [0.19, 0.20, 0.21, 0.22, 0.23, 0.24],
 	}
    
# create data frame
df = pd.DataFrame(data, columns = ["Problem", "m_objs", "A", "B", "C", "D"]) 

# save to csv
df.to_csv("sample_input.csv", index=False, sep=";")
```

Notice that you should specify the names of the columns when creating the `pf.DataFrame` object.



## Read file

Now, let's read the previous file as follows:

```python
# load csv (all columns)
df_all = pd.read_csv("sample_input.csv", sep=";")
```

This is the output:

```python
In [2]: df_all              
Out[2]: 
  Problem  m_objs     A     B     C     D
0   DTLZ1       3  0.01  0.07  0.13  0.19
1   DTLZ1       3  0.02  0.08  0.14  0.20
2   DTLZ1       5  0.03  0.09  0.15  0.21
3   DTLZ1       5  0.04  0.10  0.16  0.22
4   DTLZ1       8  0.05  0.11  0.17  0.23
5   DTLZ1       8  0.06  0.12  0.18  0.24
```

Notice that the names of the columns are defined automatically. Optionally, you could use `names=["Problem", "m_objs", "A", "B", "C", "D"]` to define the names of the columns when calling `np.read_csv`. However, this approach is impractical. An example of this issue is given at the end of the post.

Alternatively, you can read only a few columns of the input file. To do so, you must specify the names of all the specific columns in `usecols`:

```python
# load csv (some columns)
df_some = pd.read_csv("sample_input.csv", sep=";", 
                      usecols=["Problem", "m_objs", "D"])
```

This is the output:

```python
In [3]: df_some                                  
Out[3]: 
  Problem  m_objs     D
0   DTLZ1       3  0.19
1   DTLZ1       3  0.20
2   DTLZ1       5  0.21
3   DTLZ1       5  0.22
4   DTLZ1       8  0.23
5   DTLZ1       8  0.24 
```



## Read a file with comments

Now, suppose your input file has some comments, as this one, called `sample_input_with_comments.csv`:

```
# filename: sample_input_with_comments.csv
Problem;m_objs;A;B;C;D
DTLZ1;3;0.01;0.07;0.13;0.19
DTLZ1;3;0.02;0.08;0.14;0.2
DTLZ1;5;0.03;0.09;0.15;0.21
DTLZ1;5;0.04;0.1;0.16;0.22
DTLZ1;8;0.05;0.11;0.17;0.23
DTLZ1;8;0.06;0.12;0.18;0.24
```

To read this file, you must specify the number of lines to be ignored with the parameter`skiprows`. In this case, only the first line contains comments, so `skiprows=1`:

```python
# load csv with one comment in the first line (all columns)
df_all_c = pd.read_csv("sample_input_with_comments.csv", skiprows=1, sep=";")
```

This is the output:

```python
In [4]: df_all_c                                                             
Out[4]: 
  Problem  m_objs     A     B     C     D
0   DTLZ1       3  0.01  0.07  0.13  0.19
1   DTLZ1       3  0.02  0.08  0.14  0.20
2   DTLZ1       5  0.03  0.09  0.15  0.21
3   DTLZ1       5  0.04  0.10  0.16  0.22
4   DTLZ1       8  0.05  0.11  0.17  0.23
5   DTLZ1       8  0.06  0.12  0.18  0.24
```



## Read file with `names`

You could specify the names of the columns when reading a cvs file. To do so, define the names of the columns in `names`. Consider this example:

```python
# load csv with names (all columns)
df_all_names = pd.read_csv("sample_input.csv", 
                           names=["Problem", "m_objs", "A", "B", "C", "D"], 
                           skiprows=1, sep=";")
```

Unlike the simplest approach, `pd.read_csv("sample_input.csv", sep=";")`, here we have added two parameters: `names` and `skiprows`.

This is the output:

```python
In [6]: df_all_names                                                        
Out[6]: 
  Problem  m_objs     A     B     C     D
0   DTLZ1       3  0.01  0.07  0.13  0.19
1   DTLZ1       3  0.02  0.08  0.14  0.20
2   DTLZ1       5  0.03  0.09  0.15  0.21
3   DTLZ1       5  0.04  0.10  0.16  0.22
4   DTLZ1       8  0.05  0.11  0.17  0.23
5   DTLZ1       8  0.06  0.12  0.18  0.24
```

Despite the fact that you need to include two additional parameters, you also need to take special care of the order of the columns given in `names`. Consider this example, notice that the order of the columns is reversed:

```python
# load csv with names reversed (all columns)
df_all_names_rev = pd.read_csv("sample_input.csv", 
                               names=["D", "C", "B", "A", "m_objs", 
                                      "Problem"], 
                               skiprows=1, sep=";")
```

This is the output:

```python
In [7]: df_all_names_rev                                                
Out[7]: 
       D  C     B     A  m_objs  Problem
0  DTLZ1  3  0.01  0.07    0.13     0.19
1  DTLZ1  3  0.02  0.08    0.14     0.20
2  DTLZ1  5  0.03  0.09    0.15     0.21
3  DTLZ1  5  0.04  0.10    0.16     0.22
4  DTLZ1  8  0.05  0.11    0.17     0.23
5  DTLZ1  8  0.06  0.12    0.18     0.24
```

Now, the content is changed. This could lead to further errors. For instance, suppose that you want to know the values of the column `"Problem"`.  The output will depend on how the csv file was loaded. For the first case, the content is the expected result:

```python
# df_all = pd.read_csv("sample_input.csv", sep=";")
In [14]: df_all["Problem"]                                                   
Out[14]: 
0    DTLZ1
1    DTLZ1
2    DTLZ1
3    DTLZ1
4    DTLZ1
5    DTLZ1
Name: Problem, dtype: object
```

For the last case, the content of the column `"Problem"` are not the expected values:

```python
# df_all_names_rev = pd.read_csv(..., 
#                                names=["D", "C", "B", "A", "m_objs",
#                               "Problem"], ...)
In [15]: df_all_names_rev["Problem"]                                         
Out[15]: 
0    0.19
1    0.20
2    0.21
3    0.22
4    0.23
5    0.24
Name: Problem, dtype: float64
```

Therefore, take care when using `names`.

